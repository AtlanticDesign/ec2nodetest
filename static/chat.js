  var socket = io();
  var gotFirstData = false;

  socket.on('first data', function(data) {
    console.log(data);
      if (!gotFirstData) {
          var fdata = data.Items;
          var fdataSort = fdata.sort(function(a, b) {
              return parseFloat(b.ts) - parseFloat(a.ts);
          });

          for (var i = fdataSort.length - 1; i >= 0; i--) {
              var entry = fdataSort[i];
              var msgx = entry.things;
              if (msgx) {
                  addJamtoDom(entry);
              }
          }
          gotFirstData = true;
      }
  });

  function addJamtoDom(msgObj) {
      var d = new Date(msgObj.ts);
      var temp;
      if (msgObj.things.msg == null) {
          msgObj.things.msg = '';
      }
      if (msgObj.things.img) {
         temp = '<div class="jam image">' + msgObj.things.msg + '<img src="' + msgObj.things.img + '"/>';
      } else {
         temp = '<div class="jam texter"><div class="message">' + msgObj.things.msg;
      }
      $('#messages').prepend(temp);
  }

  socket.on('chat message', function(msg) {
      addJamtoDom(msg);
  });

  socket.on('users', function(msg) {
      console.log('users: ', msg);
      var word;

      if (msg == 1 ) {
        word = ' user';
      } else {
        word = ' users';
      }
      $('.users .numb').text(msg);
      $('.users .word').text(word);

  });


  var imgLoading = $('.image-loading');
  var imgUrl;
  var workingImg, imgReady, hasImg, msgObj;

  function resetForm() {
      imgLoading.hide();
      imgReady = false;
      hasImg = false;
      $('#m').val('');
      imgUrl = false;
      $('.image-preview').css('background-image','none');
  }



  // Requires jQuery and blueimp's jQuery.fileUpload

  // client-side validation by fileUpload should match the policy
  // restrictions so that the checks fail early
  var acceptFileType = /.*/i;
  var maxFileSize = 1000000;
  // The URL to your endpoint that maps to s3Credentials function
  var credentialsUrl = '/s3_credentials';
  // The URL to your endpoint to register the uploaded file
  var uploadUrl = '/upload';

  window.initS3FileUpload = function($fileInput) {
      $fileInput.fileupload({
          //acceptFileTypes: acceptFileType,
          //maxFileSize: maxFileSize,
          paramName: 'file',
          add: s3add,
          dataType: 'xml',
          done: onS3Done
      });
  };



  $('form').submit(function() {
      msgObj = {};


      if (($('#m').val().trim().length > 0)) {
        msgObj.msg = $('#m').val();
        msgObj.hasTxt = true;
        console.log(msgObj.msg);        
      } else {
        msgObj.hasTxt = false;
        console.log('no string', $('#m').val());
      }

      msgObj.hasImg = hasImg;
      if (hasImg) {
          msgObj.img = imgUrl;
      }

      if (msgObj.hasImg || msgObj.hasTxt) {
        console.log('has image or text');
        socket.emit('chat message', msgObj);
      } else {
        console.log('has nothin');
      }

      resetForm();
      return false;
  });






  // This function retrieves s3 parameters from our server API and appends them
  // to the upload form.
  function s3add(e, data) {
      var filename = data.files[0].name;
      var params = [];
      imgLoading.show();
      $.ajax({
          url: credentialsUrl,
          type: 'GET',
          dataType: 'json',
          data: {
              filename: filename
          },
          success: function(s3Data) {
              data.url = s3Data.endpoint_url;
              data.formData = s3Data.params;
              data.submit();
          }
      });
      return params;
  };


  function onS3Done(e, data) {
      var s3Url = $(data.jqXHR.responseXML).find('Location').text();
      var s3Key = $(data.jqXHR.responseXML).find('Key').text();
      hasImg = true;
      imgUrl = s3Url;
      $('.image-preview').css('background-image','url('+imgUrl+')');
      imgLoading.hide();
  };
