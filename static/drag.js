/* 
PLAYBACK!!!!
*/

//http://stackoverflow.com/questions/10058226/send-response-to-all-clients-except-sender-socket-io

var testmode = false;

var firstPlay = true;

var canvasWidth = 1000;
var canvasHeight = 600;

var colors = {
    cream : 0xfffff0,
    green : 0xE8F7C4,
    orange: 0xEDBC6E,
    yellow: 0xFAFCA2
};

var baseColor = colors.cream;
var buildupColor = colors.yellow;
var playingColor = colors.green;


var size = [canvasWidth, canvasHeight];
var ratio = size[0] / size[1];



var socket = io();
var renderer = PIXI.autoDetectRenderer(canvasWidth, canvasHeight,{transparent: true});
//renderer.backgroundColor = 0xFFFFF0;

var cssBg = jQuery('body');

var bunnies = [];

document.body.appendChild(renderer.view);

// create the root of the scene graph
var stage = new PIXI.Container();

console.log('RENDERER');
console.log(renderer);
console.log('STAGE');
console.log(stage);
// create a texture from an image path
var texture = PIXI.Texture.fromImage('/static/img/drag/bunny.png');
var circ = PIXI.Texture.fromImage('/static/img/drag/circ.png');
var rect = PIXI.Texture.fromImage('/static/img/drag/rect.png');
var tri = PIXI.Texture.fromImage('/static/img/drag/tri.png');
var bar = PIXI.Texture.fromImage('/static/img/drag/bar.png');

var textures = [circ,rect,tri,bar];
//console.log('textures length ' + textures.length);

var imDragging = false;
var isPlaying = false;
var fdataSort;


var bxId = 0;
    bxId = 1000;

var texNo = 0;

function createBunny(x, y, i)
{
    var bxId = i;
    //bxId++;



    if (texNo < textures.length -1) {
        texNo ++;
    } else {
        texNo = 0;
    }

    //console.log(bxId);
    //console.log(texNo, textures[texNo]);
    // create our little bunny friend..
    var bunny = new PIXI.Sprite(textures[texNo]);



    // enable the bunny to be interactive... this will allow it to respond to mouse and touch events
    bunny.interactive = false;

    // this button mode will mean the hand cursor appears when you roll over the bunny with your mouse
    bunny.buttonMode = true;
    bunny.defaultCursor = "move"; //or some othe css cursor style
    // center the bunny's anchor point
    bunny.anchor.set(0.5);

    // make it a bit bigger, so it's easier to grab
    bunny.scale.set(.5);
    bunny.alpha = 0;
    bunny.bunnyId = i;
    // setup events
    bunny
        // events for drag start
        .on('mousedown', onDragStart)
        .on('touchstart', onDragStart)
        // events for drag end
        .on('mouseup', onDragEnd)
        .on('mouseupoutside', onDragEnd)
        .on('touchend', onDragEnd)
        .on('touchendoutside', onDragEnd)
        // events for drag move
        .on('mousemove', onDragMove)
        .on('touchmove', onDragMove);

    // move the sprite to its designated position
    bunny.position.x = x;
    bunny.position.y = y;


    var isBar = false;
    if (textures[texNo] == bar) {
        console.log('got a bar');
        isBar = true;
        bunny.rotation = 0.59009582;
    }


    // add it to the stage
    stage.addChild(bunny);
    bunnies.push(bunny);
}



var bgfader = new PIXI.Graphics();

/*
bgfader.beginFill(baseColor);

// set the line style to have a width of 5 and set the color to red
//bgfader.lineStyle(5, 0xFF0000);

// draw a rectangle
bgfader.drawRect(0, 0, canvasWidth, canvasHeight);

// var bgfaderFill = bgfader.graphicsData[0].fillColor;

stage.addChild(bgfader);

console.log(bgfader);
*/
//bgfader.graphicsData[0].fillColor = 0xfffff0;


for (var i = 0; i < 8; i++)
{
    //console.log(initialPos[i].x);
    createBunny(10*i+1 , 10*i+1, i);
}

requestAnimationFrame( animate );

function animate() {


    requestAnimationFrame(animate);
    // render the stage
    renderer.render(stage);
}


function getPositionAll() {
    var currentComposition = [];
    for (var i = 0; i < bunnies.length; i++) {
        var bunObj = {};
        var bun = bunnies[i];
        bunObj.id = bun.bunnyId;
        bunObj.x = bun.position.x;
        bunObj.y = bun.position.y;
        currentComposition.push(bunObj);
    }    
    return currentComposition;
}


var pauseCheck = null;



function onDragStart(event)
{
    
    // store a reference to the data
    // the reason for this is because of multitouch
    // we want to track the movement of this particular touch
    //var bId = event.target.bunnyId;

    //console.log('on this', this.bunnyId);

    clearTimeout(pauseCheck);

    TweenMax.killTweensOf(cssBg);
    //TweenMax.to(bgfader.graphicsData[0], .3, {colorProps:{fillColor:baseColor,format:"number"}, ease:Linear.easeNone});
    TweenMax.to(cssBg, .3, {backgroundColor:baseColor, ease:Linear.easeNone});
    


    imDragging = this.bunnyId;
    
    this.data = event.data;
    this.alpha = 1;
    this.dragging = true;
    //this.defaultCursor = "grabbing"; //or some othe css cursor style

    socket.emit('drag start', this.bunnyId);
}

function onDragEnd(event)
{

    var dragData = {};
    var tooFar = false;

    imDragging = false;
    
    dragData.id = this.bunnyId;
    dragData.x = this.position.x;
    dragData.y = this.position.y;

   // console.log('W H',this.width, this.height);
    // Dont allow objects to be pushed off canvas
    if (dragData.x > canvasWidth) {
        console.log('off reservation X');
        dragData.x = canvasWidth-(this.width/2);
        tooFar = true;
    }

    if (dragData.x < 0) {
        console.log('off reservation X ---');
        dragData.x = (this.width/2);
        tooFar = true;
    }

    if (dragData.y > canvasHeight) {
        console.log('off reservation Y');
        dragData.y = canvasHeight-(this.height/2);
        tooFar = true;
    }
    
    if (dragData.y < 0) {
        console.log('off reservation Y ---');
        dragData.y = (this.height/2);
        tooFar = true;
    }

    if (tooFar) {
        this.position.x = dragData.x;
        this.position.y = dragData.y;        
    }


 
    this.alpha = 1;
    
    //this.defaultCursor = "grab"; //or some othe css cursor style

    this.dragging = false;
    // set the interaction data to null
    this.data = null;
    //console.log('drag end', dragData);

    var posAll = getPositionAll();

    var dataForServer = {};
    dataForServer.dragData = dragData;
    dataForServer.posAll = posAll;

    socket.emit('drag end', dataForServer);

    //TweenMax.to(bgfader.graphicsData[0], 2, {colorProps:{fillColor:buildupColor,format:"number"}, delay:1, ease:Linear.easeNone});
    TweenMax.to(cssBg, 2, {backgroundColor:buildupColor, delay:1, ease:Linear.easeNone});
    
    pauseCheck = setTimeout(function() {
        console.log('idle for 3 sec');
        playing(100);
        pauseCheck = null;
    }, 3000);

}

function onDragMove(event)
{
    if (this.dragging)
    {




        var newPosition = this.data.getLocalPosition(this.parent);
        this.position.x = newPosition.x;
        this.position.y = newPosition.y;



        var dragData = {};
        
        dragData.id = this.bunnyId;
        dragData.x = newPosition.x;
        dragData.y = newPosition.y;

        //console.log('drag live', dragData);
        socket.emit('drag live', dragData);

        /*
        this.alpha = 1;

        this.dragging = false;
        // set the interaction data to null
        socket.emit('drag live', dragData);


        //console.log(this.position.x);
        */
    }
}




var playing = function(numb){
    var aniInterval = 100;
    var curIndex;
    
    if (!firstPlay) {
        //TweenMax.to(bgfader.graphicsData[0], .3, {colorProps:{fillColor:playingColor,format:"number"}, ease:Linear.easeNone});
        TweenMax.to(cssBg, .3, {backgroundColor:playingColor, ease:Linear.easeNone});
    }   

    if (numb == 'all') {
        curIndex = fdataSort.length;
        console.log('play all');

    } else if (numb != undefined) {
        curIndex = numb;
        console.log('play defined', numb);
    } else {
        curIndex = 50;
        console.log('play undefined');

    }

    if (testmode) {
        curIndex = 5;
    }

    advanceComp();
    function advanceComp() {
        --curIndex;
        if (curIndex >= 0) {
            console.log('index', curIndex, fdataSort.length);

            /// COPY PASTE YO
            var comp = fdataSort[curIndex].things;
            for (var i = 0; i < comp.length; i++)
            {
                console.log('moving');
                var key = comp[i];
                var sprt = bunnies[key.id];
                sprt.alpha = 1;
                //sprt.interactive = false;
                //sprt.position.x = key.x;
                //sprt.position.y = key.y;
                TweenMax.to(sprt.position, .1, {x:key.x , y:key.y});

            }



           setTimeout(advanceComp, aniInterval);
        } else {
            firstPlay = false;
            curIndex = fdataSort.length;
            console.log('done with this animation');
            //TweenMax.to(bgfader.graphicsData[0], .2, {colorProps:{fillColor:baseColor,format:"number"}, ease:Linear.easeNone});
            TweenMax.to(cssBg, .2, {backgroundColor:baseColor, ease:Linear.easeNone});
    
        }
    }
}


function formatMsg(obj) {
    var tn = Date.now();
    var obx = {
            "ts": tn,
            "things": obj,
            "localGuy": true
        }
    return obx;
}


socket.emit('get bunny positions');

socket.on('send bunny positions', function(data) {
    //console.log('send bunny positions', data);

    // GET ALL FROM DB AND SORT
    var fdata = data.Items;
    fdataSort = fdata.sort(function(a, b) {
        return parseFloat(b.ts) - parseFloat(a.ts);
    });

   // console.log(fdataSort.length);
   // console.log(fdataSort);

    // POSITION THE COMPOSITION
    var comp = fdataSort[0].things;
    for (var i = 0; i < comp.length; i++)
    {
       // console.log('xx', comp[i].id);
        var key = comp[i];
        var sprt = bunnies[key.id];
        //console.log(sprt, key.x, key.y);
        sprt.interactive = true;
        sprt.alpha = 1;
        sprt.position.x = key.x;
        sprt.position.y = key.y;
    }

    setTimeout(playing,300);

});



socket.on('update drag', function(msg) {
    console.log('update drag', msg);

    var id = msg.dragData.id;
    var sprt = bunnies[id];
    /*
    sprt.position.x = msg.x;
    sprt.position.y = msg.y;
    */
    sprt.scale.set(.5);
    sprt.interactive = true;    
    sprt.alpha = 1;
    
    fdataSort.unshift(formatMsg(msg.posAll));

    console.log(sprt);  
});



socket.on('flag drag', function(msg) {
    console.log('flag drag', msg);
    var id = msg;
    var sprt = bunnies[id];

    // IF someone else is dragging, turn off interactivity;
    if (imDragging != id) {
        sprt.interactive = false;
    }

    sprt.alpha = .2;
    sprt.scale.set(.5);
    console.log(sprt);  
});


socket.on('draging live', function(msg) {
   // console.log('draging live', msg);
    var id = msg.id;
    var sprt = bunnies[id];
    sprt.position.x = msg.x;
    sprt.position.y = msg.y;    
});



function resize() {
    var w, h, vCent;
    var wh = window.innerHeight;
    var ww = window.innerWidth;
    var isBig = false
    if (ww > canvasHeight && wh > canvasWidth) {
        console.log('IS BIG');
        isBig = true;
    }
    if (isBig) {
        console.log('we jam');
        w = canvasWidth;
        h = canvasWidth;
        vCent = (wh - h) / 2;        
    } else if ((ww / wh >= ratio) && !isBig) {
        console.log('window ratio is extra horizontal space');
        w = wh * ratio;
        h = wh;
        vCent = 0;
    } else {
        console.log('window ratio is has extra vertical space');        
        w = ww;
        h = ww / ratio;
        vCent = (wh - h) / 2;
    }
    renderer.view.style.width = w + 'px';
    renderer.view.style.height = h + 'px';
    renderer.view.style.marginTop = vCent + 'px';

}


resize();

window.onresize = resize;