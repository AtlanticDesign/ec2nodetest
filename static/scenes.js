var socket = io();
var scene;

socket.on('new scene', function(msg) {
	if (scene != msg) {
		launchScene(msg);
	}
	scene = msg;
	console.log('got new scene', msg);
	$('.scenename').text(msg);
});

function killScenes(){
	console.log('killScenes');
	$('.chat').hide();
}

function launchScene(scene) {
	killScenes();
	console.log('launch scene', scene);

	if (scene == 'scene 1') {
		goDrag();
	}

	if (scene == 'scene 2') {
		goChat();
	}

	if (scene == 'scene 3') {
		console.log('scene 3 yo');
	}

}

function goDrag(){
	var renderer = PIXI.autoDetectRenderer(800, 600);
	var bunnies = [];

	document.body.appendChild(renderer.view);

	// create the root of the scene graph
	var stage = new PIXI.Container();

	// create a texture from an image path
	var texture = PIXI.Texture.fromImage('/static/img/drag/bunny.png');

	for (var i = 0; i < 5; i++)
	{
	    createBunny(Math.floor(Math.random() * 800) , Math.floor(Math.random() * 600),i);
	}

	var bxId = 0;
	    bxId = 1000;

	function createBunny(x, y, i)
	{
	    var bxId = i;
	    //bxId++;
	    console.log(bxId);
	    // create our little bunny friend..
	    var bunny = new PIXI.Sprite(texture);

	    // enable the bunny to be interactive... this will allow it to respond to mouse and touch events
	    bunny.interactive = true;

	    // this button mode will mean the hand cursor appears when you roll over the bunny with your mouse
	    bunny.buttonMode = true;

	    // center the bunny's anchor point
	    bunny.anchor.set(0.5);

	    // make it a bit bigger, so it's easier to grab
	    bunny.scale.set(1);

	    bunny.bunnyId = i;
	    // setup events
	    bunny
	        // events for drag start
	        .on('mousedown', onDragStart)
	        .on('touchstart', onDragStart)
	        // events for drag end
	        .on('mouseup', onDragEnd)
	        .on('mouseupoutside', onDragEnd)
	        .on('touchend', onDragEnd)
	        .on('touchendoutside', onDragEnd)
	        // events for drag move
	        .on('mousemove', onDragMove)
	        .on('touchmove', onDragMove);

	    // move the sprite to its designated position
	    bunny.position.x = x;
	    bunny.position.y = y;

	    // add it to the stage
	    stage.addChild(bunny);
	    bunnies.push(bunny);
	}

	requestAnimationFrame( animate );

	function animate() {

	    requestAnimationFrame(animate);

	    // render the stage
	    renderer.render(stage);
	}

	function onDragStart(event)
	{
	    console.log(bunnies);
	    // store a reference to the data
	    // the reason for this is because of multitouch
	    // we want to track the movement of this particular touch
	    var bId = event.target.bunnyId;

	    //console.log('on', event.target.bunnyId);

	    this.data = event.data;
	    this.alpha = 1;
	    this.dragging = true;
	    this.amItheHamone = "you know im the hammer!!!!!";

	    socket.emit('drag start', this.bunnyId);
	}

	function onDragEnd(event)
	{    
	    var dragData = {};
	    
	    dragData.id = this.bunnyId;
	    dragData.x = this.position.x;
	    dragData.y = this.position.y;

	    this.alpha = 1;

	    this.dragging = false;
	    // set the interaction data to null
	    this.data = null;
	    console.log('drag end', dragData);
	    socket.emit('drag end', dragData);

	}

	function onDragMove(event)
	{
	    if (this.dragging)
	    {




	        var newPosition = this.data.getLocalPosition(this.parent);
	        this.position.x = newPosition.x;
	        this.position.y = newPosition.y;

	        /*

	        var dragData = {};
	        
	        dragData.id = this.bunnyId;
	        dragData.x = newPosition.x;
	        dragData.y = newPosition.y;

	        this.alpha = 1;

	        this.dragging = false;
	        // set the interaction data to null
	        console.log('drag live', dragData);
	       // socket.emit('drag live', dragData);


	        //console.log(this.position.x);
	        */
	    }
	}


	socket.on('update drag', function(msg) {
	    console.log('update drag', msg);
	    var id = msg.id;
	    var sprt = bunnies[id];
	    sprt.position.x = msg.x;
	    sprt.position.y = msg.y;
	    sprt.scale.set(2);
	    //sprt.interactive = true;    
	    sprt.alpha = 1;
	    console.log(sprt);  
	});



	socket.on('flag drag', function(msg) {
	    console.log('flag drag', msg);
	    var id = msg;
	    var sprt = bunnies[id];
	    sprt.alpha = .2;
	    //sprt.interactive = false;
	    sprt.scale.set(3);
	    console.log(sprt);  
	});

};



function goChat() {
  var gotFirstData = false;

  $('.chat').show();

  socket.on('first data', function(data) {
    console.log(data);
      if (!gotFirstData) {
          var fdata = data.Items;
          var fdataSort = fdata.sort(function(a, b) {
              return parseFloat(b.ts) - parseFloat(a.ts);
          });

          for (var i = fdataSort.length - 1; i >= 0; i--) {
              var entry = fdataSort[i];
              var msgx = entry.things;
              if (msgx) {
                  addJamtoDom(entry);
              }
          }
          gotFirstData = true;
      }
  });

  function addJamtoDom(msgObj) {
      var d = new Date(msgObj.ts);
      var temp;
      if (msgObj.things.msg == null) {
          msgObj.things.msg = '';
      }
      if (msgObj.things.img) {
         temp = '<div class="jam image">' + msgObj.things.msg + '<img src="' + msgObj.things.img + '"/>';
      } else {
         temp = '<div class="jam texter"><div class="message">' + msgObj.things.msg;
      }
      $('#messages').prepend(temp);
  }

  socket.on('chat message', function(msg) {
      addJamtoDom(msg);
  });

  socket.on('users', function(msg) {
      console.log('users: ', msg);
      var word;

      if (msg == 1 ) {
        word = ' user';
      } else {
        word = ' users';
      }
      $('.users .numb').text(msg);
      $('.users .word').text(word);

  });


  var imgLoading = $('.image-loading');
  var imgUrl;
  var workingImg, imgReady, hasImg, msgObj;

  function resetForm() {
      imgLoading.hide();
      imgReady = false;
      hasImg = false;
      $('#m').val('');
      imgUrl = false;
      $('.image-preview').css('background-image','none');
  }



  // Requires jQuery and blueimp's jQuery.fileUpload

  // client-side validation by fileUpload should match the policy
  // restrictions so that the checks fail early
  var acceptFileType = /.*/i;
  var maxFileSize = 1000000;
  // The URL to your endpoint that maps to s3Credentials function
  var credentialsUrl = '/s3_credentials';
  // The URL to your endpoint to register the uploaded file
  var uploadUrl = '/upload';

  window.initS3FileUpload = function($fileInput) {
      $fileInput.fileupload({
          //acceptFileTypes: acceptFileType,
          //maxFileSize: maxFileSize,
          paramName: 'file',
          add: s3add,
          dataType: 'xml',
          done: onS3Done
      });
  };



  $('form').submit(function() {
      msgObj = {};


      if (($('#m').val().trim().length > 0)) {
        msgObj.msg = $('#m').val();
        msgObj.hasTxt = true;
        console.log(msgObj.msg);        
      } else {
        msgObj.hasTxt = false;
        console.log('no string', $('#m').val());
      }

      msgObj.hasImg = hasImg;
      if (hasImg) {
          msgObj.img = imgUrl;
      }

      if (msgObj.hasImg || msgObj.hasTxt) {
        console.log('has image or text');
        socket.emit('chat message', msgObj);
      } else {
        console.log('has nothin');
      }

      resetForm();
      return false;
  });






  // This function retrieves s3 parameters from our server API and appends them
  // to the upload form.
  function s3add(e, data) {
      var filename = data.files[0].name;
      var params = [];
      imgLoading.show();
      $.ajax({
          url: credentialsUrl,
          type: 'GET',
          dataType: 'json',
          data: {
              filename: filename
          },
          success: function(s3Data) {
              data.url = s3Data.endpoint_url;
              data.formData = s3Data.params;
              data.submit();
          }
      });
      return params;
  };


  function onS3Done(e, data) {
      var s3Url = $(data.jqXHR.responseXML).find('Location').text();
      var s3Key = $(data.jqXHR.responseXML).find('Key').text();
      hasImg = true;
      imgUrl = s3Url;
      $('.image-preview').css('background-image','url('+imgUrl+')');
      imgLoading.hide();
  };


  $(function() {
    initS3FileUpload($('#fileInput'));
  });


var card = $('#card');
var bc = $('.bc-img');
var fc = $('#messages');

bc.click(function(){
    card.removeClass('flipped');
    setTimeout(function(){
		bc.css('background-image','none');    
    },500);
});

fc.on('click', '.jam img', function(){
	var src = $(this).attr('src');
	var iw, ih;	
	var ww = $(window).width();
	var wh = $(window).height();

	$("<img/>") // Make in memory copy of image to avoid css issues
	    .attr("src", src)
	    .load(function() {
	        iw = this.width;   // Note: $(this).width() will not
	        ih = this.height; // work for in memory images.
			console.log(ww,wh,iw,ih);
			if (ww < iw || wh < ih) {
				bc.css('background-size','contain');
			} else {
				bc.css('background-size','auto');
			}

			bc.css('background-image','url('+src+')');
		    card.addClass('flipped');	        
	    });

});

fc.on('click', '.jam.texter', function(){
	var me = $(this);
	me.toggleClass('bigtext').siblings().removeClass('bigtext');
});




}