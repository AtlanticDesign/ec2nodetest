var card = $('#card');
var bc = $('.bc-img');
var fc = $('#messages');

bc.click(function(){
    card.removeClass('flipped');
    setTimeout(function(){
		bc.css('background-image','none');    
    },500);
});

fc.on('click', '.jam img', function(){
	var src = $(this).attr('src');
	var iw, ih;	
	var ww = $(window).width();
	var wh = $(window).height();

	$("<img/>") // Make in memory copy of image to avoid css issues
	    .attr("src", src)
	    .load(function() {
	        iw = this.width;   // Note: $(this).width() will not
	        ih = this.height; // work for in memory images.
			console.log(ww,wh,iw,ih);
			if (ww < iw || wh < ih) {
				bc.css('background-size','contain');
			} else {
				bc.css('background-size','auto');
			}

			bc.css('background-image','url('+src+')');
		    card.addClass('flipped');	        
	    });

});

fc.on('click', '.jam.texter', function(){
	var me = $(this);
	me.toggleClass('bigtext').siblings().removeClass('bigtext');
});