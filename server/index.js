var AWS = require("aws-sdk");
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var crypto = require('crypto');
var path = require('path');

var s3 = require('./s3');
var dbe = require('./db');

var chatTable = "jambonerJah";
var dragTable = "draggers";



var s3Config = {
  accessKey: process.env.S3_ACCESS_KEY,
  secretKey: process.env.S3_SECRET_KEY,
  bucket: process.env.S3_BUCKET,
  region: process.env.S3_REGION
};

AWS.config.update({
    region: "us-west-2",
    endpoint: "https://dynamodb.us-west-2.amazonaws.com"
});

app.get('/s3_credentials', function(request, response) {
  if (request.query.filename) {
    var filename =
      crypto.randomBytes(16).toString('hex') +
      path.extname(request.query.filename);
    response.json(s3.s3Credentials(s3Config, filename));
  } else {
    response.status(400).send('filename is required');
  }
});

var dbdc = new AWS.DynamoDB.DocumentClient();


function formatMsg(obj) {
    var tn = Date.now();
    var obx = {
            "ts": tn,
            "things": obj
        }
    return obx;
}

function addItemToDb(obj,table) {
    var params = {
        TableName: table,
        Item: obj
    };

    dbdc.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            //console.log("Added item:", JSON.stringify(data, null, 2));
            //console.log("Added item! ts:", obj.ts, "things.length:", obj.things.length);
            console.log("Added item! ts:", obj);
        }
    });
}

var ds;

function readDb(table, command) {

    var params = {
        TableName: table
    };

    dbdc.scan(params, function(err, data) {
        if (err)
            console.log('DB ERROR!!! ', JSON.stringify(err, null, 2));
        else
           // console.log('DB SUCCESS!!! ', data, table);
            console.log('DB SUCCESS', table);
            ds = data;
            if (command) {
                io.emit(command, data);
               // console.log('EMIT COMMAND', command, data);
            }
            return ds;
    });

}
 
app.use('/static', express.static(__dirname + '/../static'));


app.get('/chat', function(req, res) {
    res.sendfile('client/index.html');

});

app.get('/drag', function(req, res) {
    res.sendfile('client/drag.html');
});


app.get('/scenes', function(req, res) {
    res.sendfile('client/scenes.html');
});


var totalUsers = 0;
var clients;

var scenes = ['scene 1', 'scene 2', 'scene 3'];

var theScene = 0;

function sendScene() {
    //console.log('new scene',  scenes[theScene]);
    io.emit('new scene',  scenes[theScene]);
}


// DRAG APP



var minutes = .25, the_interval = minutes * 60 * 1000;
setInterval(function() {
  
  if (theScene == scenes.length-1) {
    theScene = 0;
  } else {
    theScene++;
  }

  sendScene();

}, the_interval);



io.on('connection', function(socket) {
    totalUsers++;

    sendScene();
    
    clients = io.sockets.clients().length;
    console.log('a user connected, total users: ', totalUsers, ' clients: ', clients);
   
   /* 
    var dbdata = readDb(chatTable);
    io.emit('first data', ds);
    io.emit('users', totalUsers);
  */
    socket.on('disconnect', function() {
        totalUsers--;
        clients = io.sockets.clients().length;
        console.log('user disconnected, total users: ', totalUsers, ' clients: ', clients);
        io.emit('users', totalUsers);
    });

    // CHAT APP
    socket.on('chat message', function(msg) {
        var xmsg = formatMsg(msg);
        io.emit('chat message', xmsg);
        addItemToDb(xmsg,chatTable);
    });


    // DRAG APP
    socket.on('drag end', function(msg){
        //console.log(' DRAG MESSAGE SENDING:',msg,xmsg);          
        
        console.log('GOT THE DATA!!!');
        console.log(msg);

        var xmsg = formatMsg(msg.posAll);      
        addItemToDb(xmsg,dragTable,dbdc);        
        
        io.emit('update drag', msg);
    });

    socket.on('drag start', function(msg){
        io.emit('flag drag', msg);
    });

    socket.on('get bunny positions', function(msg){
        console.log('gbp', msg);
        var dbread = readDb(dragTable, 'send bunny positions',dbdc, function(){
       // var dbread = dbe.readDb(dragTable, 'send bunny positions',dbdc, function(){
            console.log('fuck it', dbread);    
        });
        
    });

    /*
    socket.on('update posAll', function(msg){
        var xmsg = formatMsg(msg);
        addItemToDb(xmsg,dragTable,dbdc);        
    });
    */
    socket.on('drag live', function(msg){
        io.emit('draging live', msg);
        console.log('drag live', msg);
    });



      

});



http.listen(3000, function() {
    console.log('listening on *:3000');  
});
