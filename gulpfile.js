// INCLUDE GULP
var gulp = require('gulp');
//var exec =  require('child_process').exec;

// INCLUDE PLUGINS
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minify = require('gulp-minify');
var compass = require('gulp-compass');
var plumber = require('gulp-plumber');


// LINT JS
gulp.task('lint', function() {
    return gulp.src('static/js-src/*.js')
        .pipe(jshint())
        .pipe(plumber())
        .pipe(jshint.reporter('default'))       
});

// COMPILE SASS

gulp.task('compass', function() {
  return gulp.src('static/scss/*.scss')
    .pipe(compass({
      config_file: 'static/scss/config.rb',
      css: 'static/css',
      sass: 'static/scss'
    }))
    .on('error', function(error) {
      console.log('COMPASS ERROR');
      this.emit('end')
    })    
    .pipe(gulp.dest('static/css'));
});



// CONCATENATE & MINIFY JS
gulp.task('scripts', function() {

    return gulp.src([
            'static/js-src/*.js',

        ])      
        .pipe(concat('scripts.js'))
        .on('error', function(error) {
          // Would like to catch the error here 
          //console.log(error);
          //console.log(error.toString())
          console.log('SCRIPTS ERROR');

          this.emit('end')

        })         
        .pipe(gulp.dest('static/js'))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify({
            drop_console: true, // <- removes console.log 
            hoist_funs: false // 
        }))
        .pipe(gulp.dest('static/js'));

});




// WATCH FILES FOR CHANGES
gulp.task('watch', function() {
    gulp.watch('static/js-src/*.js', ['lint', 'scripts']);
    gulp.watch('static/scss/**/*.scss', ['compass']);
});

// DEFAULT TASK
gulp.task('default', ['lint', 'compass', 'scripts', 'watch']);
